//
//  SupportVC.m
//  UberNew
//
//  Created by Elluminati - macbook on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "HistoryVC.h"
#import "HistoryCell.h"
#import "Constants.h"
#import "UIImageView+Download.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "UtilityClass.h"
#import "UberStyleGuide.h"
#import "subTypeCell.h"
#import "SWRevealViewController.h"

@interface HistoryVC ()
{
    NSMutableArray *arrHistory;
    NSMutableArray *arrForDate;
    NSMutableArray *arrForSection ,*arrSubType;
    NSMutableString *strForCurrency;
}

@end

@implementation HistoryVC

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self SetLocalization];
    [self customSetup];
    self.viewForBill.hidden=YES;
    arrHistory=[[NSMutableArray alloc]init];
    //[super setNavBarTitle:TITLE_SUPPORT];
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"GETTING HISTORY", nil)];
    [self getHistory];
    
    /*self.btnMenu.titleLabel.font=[UberStyleGuide fontRegular];
    
    self.lblDistCost.font=[UberStyleGuide fontRegular];
    self.lblBasePrice.font=[UberStyleGuide fontRegular];
    
    self.lblPerDist.font=[UberStyleGuide fontRegular];
    self.lblPerTime.font=[UberStyleGuide fontRegular];
    self.lblTimeCost.font=[UberStyleGuide fontRegular];
    self.lblPomoBouns.font=[UberStyleGuide fontRegular];
    self.lblReferralBouns.font=[UberStyleGuide fontRegular];*/
    
    //self.lblTotal.font=[UberStyleGuide fontRegular:25.0f];
  
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.tableView.hidden=NO;
    self.viewForBill.hidden=YES;
    self.lblnoHistory.hidden=YES;
    self.imgNoDisplay.hidden=YES;
    self.navigationController.navigationBarHidden=NO;
}
- (void)viewDidAppear:(BOOL)animated
{
    [self.btnMenu setTitle:NSLocalizedString(@"History", nil) forState:UIControlStateNormal];
}
-(void)viewWillDisappear:(BOOL)animated
{
    isMenuOpen=NO;
}
-(void)SetLocalization
{
    self.lblInvoice.text=NSLocalizedString(@"Invoice", nil);
    self.lBasePrice.text=NSLocalizedString(@"BASE PRICE", nil);
    self.lDistanceCost.text=NSLocalizedString(@"DISTANCE COST", nil);
    self.lTimeCost.text=NSLocalizedString(@"TIME COST", nil);
    self.lPromoBonus.text=NSLocalizedString(@"PROMO BOUNCE", nil);
    self.lreferalBonus.text=NSLocalizedString(@"REFERRAL BOUNCE", nil);
    self.lTotalCost.text=NSLocalizedString(@"Total Due", nil);
    [self.btnClose setTitle:NSLocalizedString(@"Close",nil) forState:UIControlStateNormal];
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        //[self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}

#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark-
#pragma mark - Table view data source

-(void)makeSection
{
    arrForDate=[[NSMutableArray alloc]init];
    arrForSection=[[NSMutableArray alloc]init];
    NSMutableArray *arrtemp=[[NSMutableArray alloc]init];
    [arrtemp addObjectsFromArray:arrHistory];
    NSSortDescriptor *distanceSortDiscriptor = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO
                                                                            selector:@selector(localizedStandardCompare:)];
    
    [arrtemp sortUsingDescriptors:@[distanceSortDiscriptor]];
    
    for (int i=0; i<arrtemp.count; i++)
    {
        NSMutableDictionary *dictDate=[[NSMutableDictionary alloc]init];
        dictDate=[arrtemp objectAtIndex:i];
        
        NSString *temp=[dictDate valueForKey:@"date"];
        NSArray *arrDate=[temp componentsSeparatedByString:@" "];
        NSString *strdate=[arrDate objectAtIndex:0];
        if(![arrForDate containsObject:strdate])
        {
            [arrForDate addObject:strdate];
        }
    }
    for (int j=0; j<arrForDate.count; j++)
    {
        NSMutableArray *a=[[NSMutableArray alloc]init];
        [arrForSection addObject:a];
    }
    for (int j=0; j<arrForDate.count; j++)
    {
        NSString *strTempDate=[arrForDate objectAtIndex:j];
        
        for (int i=0; i<arrtemp.count; i++)
        {
            NSMutableDictionary *dictSection=[[NSMutableDictionary alloc]init];
            dictSection=[arrtemp objectAtIndex:i];
            NSArray *arrDate=[[dictSection valueForKey:@"date"] componentsSeparatedByString:@" "];
            NSString *strdate=[arrDate objectAtIndex:0];
            if ([strdate isEqualToString:strTempDate])
            {
                [[arrForSection objectAtIndex:j] addObject:dictSection];
            }
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if(tableView.tag==1)
    {
        return arrForSection.count;
    }
    else
    {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag == 1)
    {
        return  [[arrForSection objectAtIndex:section] count];
    }
    else
    {
        return arrSubType.count;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView.tag == 1)
    {
        return 20.0f;
    }
    else
    {
        return 0.0f;
    }
}

/*-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(tableView.tag == 1)
    {
        return 1.0;
    }
    else
    {
        return 0.0f;
    }
}*/

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if(tableView.tag == 1)
    {
        UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 15)];
        UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 300, 15)];
        lblDate.font=[UberStyleGuide fontRegular:13.0f];
        lblDate.textColor=[UberStyleGuide colorDefault];
        NSString *strDate=[arrForDate objectAtIndex:section];
        NSString *current=[[UtilityClass sharedObject] DateToString:[NSDate date] withFormate:@"yyyy-MM-dd"];
        
        ///   YesterDay Date Calulation
        
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = -1;
        NSDate *yesterday = [gregorian dateByAddingComponents:dayComponent
                                                       toDate:[NSDate date]
                                                      options:0];
        NSString *strYesterday=[[UtilityClass sharedObject] DateToString:yesterday withFormate:@"yyyy-MM-dd"];
        
        if([strDate isEqualToString:current])
        {
            lblDate.text=@"Today";
            lblDate.font=[UberStyleGuide fontRegularBold:10.0f];
            headerView.backgroundColor=[UIColor clearColor];
            lblDate.textColor=[UIColor darkGrayColor];
        }
        else if ([strDate isEqualToString:strYesterday])
        {
            lblDate.text=@"Yesterday";
            lblDate.textColor=[UIColor darkGrayColor];
            lblDate.font=[UberStyleGuide fontRegularBold:10.0f];
            headerView.backgroundColor=headerView.backgroundColor=[UIColor clearColor];
        }
        else
        {
            NSDate *date=[[UtilityClass sharedObject]stringToDate:strDate withFormate:@"yyyy-MM-dd"];
            NSString *text=[[UtilityClass sharedObject]DateToString:date withFormate:@"dd MMMM yyyy"];
            lblDate.text=text;
            lblDate.textColor=[UIColor darkGrayColor];
            lblDate.font=[UberStyleGuide fontRegularBold:10.0f];
            headerView.backgroundColor=headerView.backgroundColor=[UIColor clearColor];
        }
        
        [headerView addSubview:lblDate];
        return headerView;
    }
    else
    {
        return nil;
    }
}

/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [arrForDate objectAtIndex:section];
}*/

/*-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(tableView.tag == 1)
    {
        UIImageView *imgFooter=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"rectangle2"]];
        return imgFooter;
    }
    else
    {
        return nil;
    }
}*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 1)
    {
        static NSString *cellIdentifier = @"historycell";
        
        HistoryCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        if (cell==nil)
        {
            cell=[[HistoryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        NSMutableDictionary *pastDict=[[arrForSection objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
        NSMutableDictionary *dictOwner=[pastDict valueForKey:@"walker"];
        
        cell.lblName.text=[NSString stringWithFormat:@"%@ %@",[dictOwner valueForKey:@"first_name"],[dictOwner valueForKey:@"last_name"]];
        cell.lblType.text=[NSString stringWithFormat:@"%@",[dictOwner valueForKey:@"phone"]];
        cell.lblPrice.text=[NSString stringWithFormat:@"%@ %.2f",[pastDict valueForKey:@"currency"],[[pastDict valueForKey:@"total"] floatValue]];
        
        strForCurrency = [pastDict valueForKey:@"currency"];
        
        cell.lblTime.text=[NSString stringWithFormat:@"%.2f",[[dictOwner valueForKey:@"rate"] floatValue]];
        [cell.imageView downloadFromURL:[dictOwner valueForKey:@"picture"] withPlaceholder:nil];
        return cell;

    }
    else
    {
        static NSString *cellIdentifier = @"subType";
        
        subTypeCell *cell = [self.tblForTypes dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        if (cell==nil)
        {
            cell=[[subTypeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        NSDictionary *dict = [arrSubType objectAtIndex:indexPath.row];
        
        NSInteger count = (arrSubType.count-indexPath.row);
    
        if(count==5)
        {
            cell.lblMainType.hidden=NO;
            cell.lblSubType.hidden=NO;
            cell.lblTypeName.hidden=YES;
            cell.lblMainType.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"name"]];
            cell.lblSubType.text = [NSString stringWithFormat:@"%@ %@ per km",[dict valueForKey:@"currency"],[dict valueForKey:@"distance_price"]];
            cell.lblPrice.text =[NSString stringWithFormat:@"%@ %.2f",[dict valueForKey:@"currency"],[[dict valueForKey:@"cost"] floatValue]];
        }
        else if (count==6)
        {
            cell.lblMainType.hidden=NO;
            cell.lblSubType.hidden=NO;
            cell.lblTypeName.hidden=YES;
            cell.lblMainType.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"name"]];
            cell.lblSubType.text = [NSString stringWithFormat:@"%@ %@ per min",[dict valueForKey:@"currency"],[dict valueForKey:@"time_price"]];
            cell.lblPrice.text =[NSString stringWithFormat:@"%@ %.2f",[dict valueForKey:@"currency"],[[dict valueForKey:@"cost"] floatValue]];
        }
        else
        {
            cell.lblMainType.hidden=YES;
            cell.lblSubType.hidden=YES;
            cell.lblTypeName.hidden=NO;
            cell.lblTypeName.text = [dict valueForKey:@"name"];
            cell.lblPrice.text =[NSString stringWithFormat:@"%@ %.2f",strForCurrency,[[dict valueForKey:@"price"] floatValue]];
        }
        
        return cell;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 1)
        return 60;
    else
        return 73;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 1)
    {
        [self.btnMenu setTitle:@"  Invoice" forState:UIControlStateNormal];
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        NSMutableDictionary *pastDict=[[arrForSection objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
        
        NSLog(@"Payment Detail:- %@",pastDict);
        
        self.lblTotal.text=[NSString stringWithFormat:@"%@ %.2f",[pastDict valueForKey:@"currency"],[[pastDict valueForKey:@"total"] floatValue]];
        
        arrSubType = [[NSMutableArray alloc]init];
        [arrSubType addObjectsFromArray:[pastDict valueForKey:@"type"]];
        
        NSMutableDictionary *dictprice = [[NSMutableDictionary alloc]init];
        
        [dictprice setValue:[pastDict valueForKey:@"price_per_unit_time"] forKey:@"time_price"];
        [dictprice setValue:NSLocalizedString(@"Time Cost :", nil) forKey:@"name"];
        [dictprice setValue:[pastDict valueForKey:@"time_cost"] forKey:@"cost"];
        [dictprice setValue:[pastDict valueForKey:@"currency"] forKey:@"currency"];
        [arrSubType addObject:dictprice];
        
        dictprice = [[NSMutableDictionary alloc]init];
        
        [dictprice setValue:[pastDict valueForKey:@"price_per_unit_distance"] forKey:@"distance_price"];
        [dictprice setValue:NSLocalizedString(@"Distance Cost :", nil) forKey:@"name"];
        [dictprice setValue:[pastDict valueForKey:@"distance_cost"] forKey:@"cost"];
        [dictprice setValue:[pastDict valueForKey:@"currency"] forKey:@"currency"];
        [arrSubType addObject:dictprice];
        
        dictprice = [[NSMutableDictionary alloc]init];
        [dictprice setValue:NSLocalizedString(@"PROMO_BONUS", nil) forKey:@"name"];
        [dictprice setValue:[pastDict valueForKey:@"promo_bonus"] forKey:@"price"];
        [dictprice setValue:[pastDict valueForKey:@"currency"] forKey:@"currency"];
        [arrSubType addObject:dictprice];
        
        NSMutableDictionary *dictReferral = [[NSMutableDictionary alloc]init];
        [dictReferral setValue:NSLocalizedString(@"REFERRAL_BONUS", nil) forKey:@"name"];
        [dictReferral setValue:[pastDict valueForKey:@"referral_bonus"] forKey:@"price"];
        [dictReferral setValue:[pastDict valueForKey:@"currency"] forKey:@"currency"];
        [arrSubType addObject:dictReferral];
        
        dictprice = [[NSMutableDictionary alloc]init];
        [dictprice setValue:NSLocalizedString(@"province_tax", nil) forKey:@"name"];
        [dictprice setValue:[pastDict valueForKey:@"province_tax"] forKey:@"price"];
        [dictprice setValue:[pastDict valueForKey:@"currency"] forKey:@"currency"];
        [arrSubType addObject:dictprice];
        
        dictprice = [[NSMutableDictionary alloc]init];
        [dictprice setValue:NSLocalizedString(@"CANADA_TAX", nil) forKey:@"name"];
        [dictprice setValue:[pastDict valueForKey:@"canada_tax"] forKey:@"price"];
        [dictprice setValue:[pastDict valueForKey:@"currency"] forKey:@"currency"];
        [arrSubType addObject:dictprice];
        
        self.tblForTypes.frame = CGRectMake(self.tblForTypes.frame.origin.x, self.tblForTypes.frame.origin.y, self.tblForTypes.frame.size.width,self.tblForTypes.frame.size.width+arrSubType.count+15.0f);
        
        [self.tblForTypes reloadData];
        
        [UIView animateWithDuration:0.5 animations:^{
            self.viewForBill.hidden=NO;
        } completion:^(BOOL finished)
         {
         }];
    }
    else
    {
        UITableViewCell *cell = [self.tblForTypes cellForRowAtIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    }
    
}
#pragma mark -
#pragma mark - Custom Methods

-(void)getHistory
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
       NSString * strForUserId=[pref objectForKey:PREF_USER_ID];
       NSString * strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_HISTORY,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             
             NSLog(@"History Data= %@",response);
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 response = [[UtilityClass sharedObject] dictionaryByReplacingNullsWithStrings:response];

                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     [APPDELEGATE hideLoadingView];
                     
                     arrHistory=[response valueForKey:@"requests"];
                     NSLog(@"History count = %lu",(unsigned long)arrHistory.count);
                     if (arrHistory.count!=0)
                     {
                         self.tableView.hidden=NO;
                         self.lblnoHistory.hidden=YES;
                         self.imgNoDisplay.hidden=YES;
                         [self makeSection];
                         [self.tableView reloadData];
                     }
                     else
                     {
                         self.tableView.hidden=YES;
                         self.lblnoHistory.hidden=NO;
                         self.imgNoDisplay.hidden=NO;
                     }
                     
                 }
                 else
                 {
                     NSString *str = [response valueForKey:@"error_code"];
                     if([str intValue] == 406)
                     {
                         [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                     }
                 }
             }
             
         }];
        
        
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No Internet" message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
}
        
- (IBAction)closeBtnPressed:(id)sender
{
    [self.btnMenu setTitle:NSLocalizedString(@"History", nil) forState:UIControlStateNormal];
    [UIView animateWithDuration:0.5 animations:^{
        self.viewForBill.hidden=YES;
    } completion:^(BOOL finished)
     {
     }];
}

@end
