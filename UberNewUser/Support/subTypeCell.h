//
//  subTypeCell.h
//  Doctor Express
//
//  Created by My Mac on 7/15/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface subTypeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTypeName;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblMainType;
@property (weak, nonatomic) IBOutlet UILabel *lblSubType;

@end
