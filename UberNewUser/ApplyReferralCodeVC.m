//
//  ApplyReferralCodeVC.m
//  UberforXOwner
//
//  Created by Deep Gami on 22/11/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "ApplyReferralCodeVC.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "AppDelegate.h"
#import "UberStyleGuide.h"

@interface ApplyReferralCodeVC ()
{
    NSString *Referral;
}

@end

@implementation ApplyReferralCodeVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:NO];
    self.viewForReferralError.hidden=YES;
    [self SetLocalization];
    self.navigationItem.hidesBackButton=YES;
    self.txtCode.font=[UberStyleGuide fontRegular];
    self.lblAddReferral.font = [UberStyleGuide fontRegular];
    self.lblMsg.font = [UberStyleGuide fontRegular];
    self.btnSubmit=[APPDELEGATE setBoldFontDiscriptor:self.btnSubmit];
    self.btnContinue=[APPDELEGATE setBoldFontDiscriptor:self.btnContinue];
    Referral=@"";
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.viewForReferralError.hidden=YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated
{
    [self.btn_Navi_Title setTitle:NSLocalizedString(@"Referral Code", nil) forState:UIControlStateNormal];
}
-(void)SetLocalization
{
    self.txtCode.placeholder=NSLocalizedString(@"Enter Referral Code", nil);
    self.lblMsg.text=NSLocalizedString(@"Referral_Msg", nil);
    [self.btnContinue setTitle:NSLocalizedString(@"SKIP", nil) forState:UIControlStateNormal];
    [self.btnContinue setTitle:NSLocalizedString(@"SKIP", nil) forState:UIControlStateSelected];
    [self.btnSubmit setTitle:NSLocalizedString(@"ADD", nil) forState:UIControlStateNormal];
    [self.btnSubmit setTitle:NSLocalizedString(@"ADD", nil) forState:UIControlStateSelected];
    self.lblAddReferral.text = NSLocalizedString(@"ADD_REFERRAL", nil);
}

- (IBAction)codeBtnPressed:(id)sender
{
    if(self.txtCode.text.length<1)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter referral code" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        Referral=@"0";
        [self createService];
    }
}

- (IBAction)ContinueBtnPressed:(id)sender
{
    Referral=@"1";
    [self createService];
}

-(void)createService
{
    self.viewForReferralError.hidden=YES;
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:strForUserId forKey:PARAM_ID];
        [dictParam setValue:self.txtCode.text forKey:PARAM_REFERRAL_CODE];
        [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
        [dictParam setValue:Referral forKey:PARAM_REFERRAL_SKIP];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_APPLY_REFERRAL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             if (response)
             {
                 response = [[UtilityClass sharedObject] dictionaryByReplacingNullsWithStrings:response];

                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSLog(@"%@",response);
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"fromRegister"];
                         [pref setValue:[response valueForKey:@"is_referee"] forKey:PREF_IS_REFEREE];
                         [pref synchronize];
                         if([Referral isEqualToString:@"0"])
                         {
                             [APPDELEGATE showToastMessage:NSLocalizedString(@"SUCESS_REFERRAL", nil)];
                             [self performSegueWithIdentifier:@"segueToAddPayment" sender:self];
                         }
                         else
                         {
                             [self performSegueWithIdentifier:@"segueToAddPayment" sender:self];
                         }
                     }
                     else
                     {
                         NSString *str = [response valueForKey:@"error_code"];
                         if([str intValue] == 406)
                         {
                             [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                         }
                     }
                 }
                 else
                 {
                     self.txtCode.text=@"";
                     self.viewForReferralError.hidden=NO;
                     self.lblReferralErrorMsg.text=[response valueForKey:@"error"];
                     self.lblReferralErrorMsg.textColor=[UIColor colorWithRed:205.0/255.0 green:0.0/255.0 blue:15.0/255.0 alpha:1];
                     NSString *str = [response valueForKey:@"error_code"];
                     if([str intValue] == 406)
                     {
                         [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                     }
                 }
             }
             
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    
    
}

#pragma mark-
#pragma mark- TextField Delegate

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtCode resignFirstResponder];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.viewForReferralError.hidden=YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

@end
