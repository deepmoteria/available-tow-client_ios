//
//  PaymentVC.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 01/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import "PaymentVC.h"
#import "PaymentViewCell.h"
#import "SWRevealViewController.h"

@interface PaymentVC ()
{
    NSString *strForStripeToken,*strForLastFour;
    NSMutableArray *arrForCards;
    NSInteger buttonTag,selectRow;
}
@end

@implementation PaymentVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self customSetup];
    arrForCards=[[NSMutableArray alloc] init];
    if([[NSUserDefaults standardUserDefaults]boolForKey:@"FirstTimeLoad"]==NO)
    {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"FirstTimeLoad"];
        [self.backButton setHidden:NO];
    }
    else
    {
        if([[NSUserDefaults standardUserDefaults]boolForKey:@"fromRegister"]==NO)
        {
            [self.backButton setHidden:NO];
        }
        else
        {
            [self.backButton setHidden:YES];
        }
    }
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self SetLocalization];
    self.viewForAddingCard.hidden=YES;
    [self getAllMyCards];
}

-(void)viewWillDisappear:(BOOL)animated
{
    isMenuOpen=NO;
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController)
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.btnMenu addTarget:self action:@selector(onClickMenuPayment) forControlEvents:UIControlEventTouchUpInside];
        //[self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}

-(void)onClickMenuPayment
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark - Localization Methods

-(void)SetLocalization
{
    self.txtCardNumber.placeholder=NSLocalizedString(@"CREDIT_CARD_NUMBER", nil);
    self.txtMonth.placeholder=NSLocalizedString(@"MM", nil);
    self.txtYear.placeholder=NSLocalizedString(@"YY", nil);
    self.txtCVC.placeholder=NSLocalizedString(@"CVV", nil);
    self.lblCardinfo.text=NSLocalizedString(@"CARD_INFO_MSG", nil);
    
    [self.btnMenu setTitle:NSLocalizedString(@"PAYMENT",nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"PAYMENT",nil) forState:UIControlStateSelected];
    [self.btnAddNewCard setTitle:NSLocalizedString(@"ADD_CARD",nil) forState:UIControlStateNormal];
    [self.btnAddNewCard setTitle:NSLocalizedString(@"ADD_CARD",nil) forState:UIControlStateSelected];
    [self.btnClose setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateNormal];
    [self.btnClose setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateSelected];
    [self.BtnAddCard setTitle:NSLocalizedString(@"ADD", nil) forState:UIControlStateNormal];
    [self.BtnAddCard setTitle:NSLocalizedString(@"ADD", nil) forState:UIControlStateSelected];
    
    [self.txtCardNumber setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtMonth setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtYear setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtCVC setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - UIbutton Action Methods

- (IBAction)onClickBtnAddNewCard:(id)sender
{
    self.txtCardNumber.text=@"";
    self.txtMonth.text=@"";
    self.txtYear.text=@"";
    self.txtCVC.text=@"";
    self.viewForAddingCard.hidden=NO;
}
- (IBAction)onClickBtnMenu:(id)sender
{
    [self.view endEditing:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)onClickBtnCloseView:(id)sender
{
    [self.view endEditing:YES];
    self.txtCardNumber.text=@"";
    self.txtMonth.text=@"";
    self.txtYear.text=@"";
    self.txtCVC.text=@"";
    self.viewForAddingCard.hidden=YES;
}
- (IBAction)onClickBtnAddCard:(id)sender
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.viewForAddingCard.frame=CGRectMake(self.viewForAddingCard.frame.origin.x,65, self.viewForAddingCard.frame.size.width, self.viewForAddingCard.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
    [self.view endEditing:YES];
    if(self.txtCardNumber.text.length<1 || self.txtMonth.text.length<1 || self.txtYear.text.length<1 || self.txtCVC.text.length<1)
    {
        if(self.txtCardNumber.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_CREDIT_CARD_NUMBER", nil)];
        }
        else if(self.txtMonth.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_MONTH", nil)];
        }
        else if(self.txtYear.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_YEAR", nil)];
        }
        else if(self.txtCVC.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLESE_CVV", nil)];
        }
    }
    else
    {
        if (![Stripe defaultPublishableKey])
        {
            [[UtilityClass sharedObject] showAlertWithTitle:NSLocalizedString(@"NO_PUBLISH_KEY", nil) andMessage:NSLocalizedString(@"PUBLIS_MESSAGE", nil)];
            
            return;
        }
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"ADDING_CARD", nil)];
        STPCard *card = [[STPCard alloc] init];
        
        NSString *strCard=[self.txtCardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        card.number =strCard;
        card.expMonth =[self.txtMonth.text integerValue];
        card.expYear = [self.txtYear.text integerValue];
        card.cvc = self.txtCVC.text;
        
        [Stripe createTokenWithCard:card completion:^(STPToken *token, NSError *error) {
            if (error)
            {
                [[AppDelegate sharedAppDelegate] hideLoadingView];
                [self hasError:error];
            }
            else
            {
                [self hasToken:token];
                [self addCardOnServer];
            }
        }];
    }
}
- (void)hasError:(NSError *)error
{
    [[UtilityClass sharedObject] showAlertWithTitle:NSLocalizedString(@"Error", @"Error") andMessage:[error.userInfo valueForKey:@"com.stripe.lib:ErrorMessageKey"]];
    [APPDELEGATE hideLoadingView];
}

- (void)hasToken:(STPToken *)token
{
    strForLastFour=token.card.last4;
    strForStripeToken=token.tokenId;
}
-(void)addCardOnServer
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:strForUserId forKey:PARAM_ID];
        [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
        [dictParam setValue:strForStripeToken forKey:PARAM_STRIPE_TOKEN];
        [dictParam setValue:strForLastFour forKey:PARAM_LAST_FOUR];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_ADD_CARD withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [APPDELEGATE hideLoadingView];
             if(response)
             {
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if ([[response valueForKey:@"success"] boolValue])
                 {
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"ADD_CARD_SUCCESS", nil)];
                     self.txtCardNumber.text=@"";
                     self.txtMonth.text=@"";
                     self.txtYear.text=@"";
                     self.txtCVC.text=@"";
                     self.viewForAddingCard.hidden=YES;
                     [self getAllMyCards];
                     [self.navigationController popToRootViewControllerAnimated:YES];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_messages"]];
                     if([str intValue] == 406)
                     {
                         [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                     }
                     else
                     {
                         NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:str];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
    
}

#pragma mark -
#pragma mark - TextFieldDelegate Methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.txtCardNumber)
    {
        if (string==nil || [string isEqualToString:@""] || [string isKindOfClass:[NSNull class]] || string.length < 1)
        {
            
        }
        else if(self.txtCardNumber.text.length==4 || self.txtCardNumber.text.length==9 || self.txtCardNumber.text.length==14)
        {
            NSString *str=self.txtCardNumber.text;
            self.txtCardNumber.text=[NSString stringWithFormat:@"%@ ",str];
        }
        
        if (self.txtCardNumber.text.length == Card_Length && range.length == 0)
        {
            if(self.txtMonth.text.length >= Card_Month)
            {
                return NO;
            }
            else
            {
                [self.txtMonth becomeFirstResponder];
            }
        }
    }
    else if (textField == self.txtMonth)
    {
        if (self.txtMonth.text.length >= Card_Month && range.length == 0)
        {
            if(self.txtYear.text.length >= Card_Year)
            {
                return NO;
            }
            else
            {
                [self.txtYear becomeFirstResponder];
            }
        }
        
    }
    else if (textField == self.txtYear)
    {
        if (self.txtYear.text.length >= Card_Year && range.length == 0)
        {
            if(self.txtCVC.text.length >= Card_CVC_CVV)
            {
                return NO;
            }
            else
            {
                [self.txtCVC becomeFirstResponder];
            }
        }
    }
    else
    {
        if (self.txtCVC.text.length >= Card_CVC_CVV && range.length == 0)
        {
            [self.txtCVC resignFirstResponder];
            [UIView animateWithDuration:0.5 animations:^{
                
                self.viewForAddingCard.frame=CGRectMake(self.viewForAddingCard.frame.origin.x,65, self.viewForAddingCard.frame.size.width, self.viewForAddingCard.frame.size.height);
                
            } completion:^(BOOL finished)
             {
             }];
        }
    }
    
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtCardNumber resignFirstResponder];
    [self.txtCVC resignFirstResponder];
    [self.txtMonth resignFirstResponder];
    [self.txtYear resignFirstResponder];
    [UIView animateWithDuration:0.5 animations:^{
        
        self.viewForAddingCard.frame=CGRectMake(self.viewForAddingCard.frame.origin.x,65, self.viewForAddingCard.frame.size.width, self.viewForAddingCard.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
}

#pragma mark -
#pragma mark - GetAll Saved Card

-(void)getAllMyCards
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"GETTING_CARDS", nil)];
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_GET_CARDS,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"All Card = %@",response);
             if (response)
             {
                 response = [[UtilityClass sharedObject] dictionaryByReplacingNullsWithStrings:response];

                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"fromRegister"];
                     [arrForCards removeAllObjects];
                     [arrForCards addObjectsFromArray:[response valueForKey:@"payments"]];
                     if(arrForCards.count==0)
                     {
                         self.img_Display.hidden=NO;
                         [self.TableObj reloadData];
                     }
                     else
                     {
                         [self.TableObj reloadData];
                         self.img_Display.hidden=YES;
                     }
                 }
                 else
                 {
                     [arrForCards removeAllObjects];
                     [self.TableObj reloadData];
                     self.img_Display.hidden=NO;
                     
                     NSString *str = [response valueForKey:@"error_code"];
                     if([str intValue] == 406)
                     {
                         [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                     }
                     else
                     {
                         if([[NSUserDefaults standardUserDefaults]boolForKey:@"fromRegister"]==YES)
                         {
                             [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:[response valueForKey:@"error"]];
                         }
                         /*else
                         {
                             [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:[response valueForKey:@"error"]];
                         }*/
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

#pragma mark -
#pragma mark - UITableView Delegate Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(arrForCards.count==1)
    {
        return 1;
    }
    else if (arrForCards.count>1)
    {
        //return 2;
        return  arrForCards.count;
    }
    else
    {
        return 0;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
    {
        return 1;
    }
    else if (section==1)
    {
        return arrForCards.count-1;
    }
    else
    {
        return 0;
    }
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor=[UIColor colorWithRed:38.0/255.0f green:38.0/255.0f blue:38.0/255.0f alpha:1];
    [label setFont:[UIFont fontWithName:@"Open Sans" size:15]];
    
    if(section==0)
    {
        return label.text = NSLocalizedString(@"SELECTED_CARD", nil);
    }
    else if (section==1)
    {
        return label.text = NSLocalizedString(@"OTHER_CARD", nil);
    }
    else
    {
        return nil;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0f;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10,20, tableView.frame.size.width, 30)];
    label.textColor=[UIColor colorWithRed:38.0/255.0f green:38.0/255.0f blue:38.0/255.0f alpha:1];
    [label setFont:[UIFont fontWithName:@"AvenirLTStd-Black" size:15]];
    if(section==0)
    {
        [label setText:NSLocalizedString(@"SELECTED_CARD", nil)];
    }
    else
    {
        [label setText:NSLocalizedString(@"OTHER_CARD", nil)];
    }
    [view addSubview:label];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PaymentViewCell *cell=[self.TableObj dequeueReusableCellWithIdentifier:@"PaymentCell" forIndexPath:indexPath];
    if(arrForCards.count>0)
    {
        NSMutableDictionary *dictInfo;
        NSInteger tag;
        if(indexPath.section==0)
        {
            dictInfo=[arrForCards objectAtIndex:indexPath.row];
            tag=indexPath.row+1;
            cell.cellCardNumber.textColor=[UIColor blackColor];
            [cell.btnCardDelete setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            cell.cellImg.image=[UIImage imageNamed:@"img_card_select"];
        }
        else
        {
            dictInfo=[arrForCards objectAtIndex:indexPath.row+1];
            tag=indexPath.row+2;
            cell.cellCardNumber.textColor=[UIColor colorWithRed:179.0/255.0f green:179.0/255.0f blue:179.0/255.0f alpha:1];
            [cell.btnCardDelete setTitleColor:[UIColor colorWithRed:179.0/255.0f green:179.0/255.0f blue:179.0/255.0f alpha:1] forState:UIControlStateNormal];
            cell.cellImg.image=[UIImage imageNamed:@"img_card"];
        }
        cell.cellCardNumber.text=[NSString stringWithFormat:@"   ************ %@",[dictInfo valueForKey:@"last_four"]];
        [cell.btnCardDelete setTitle:NSLocalizedString(@"REMOVE", nil) forState:UIControlStateNormal];
        [cell.btnCardDelete setTitleColor:[UIColor colorWithRed:97.0/255.0f green:97.0/255.0f blue:97.0/255.0f alpha:1] forState:UIControlStateNormal];
        [cell.btnCardDelete setTitleColor:[UIColor colorWithRed:97.0/255.0f green:97.0/255.0f blue:97.0/255.0f alpha:1] forState:UIControlStateHighlighted];
        [cell.btnCardDelete setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [cell.btnCardDelete setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
        cell.cellCardNumber.textColor = [UIColor darkGrayColor];
        cell.btnCardDelete.tag=tag;
        [cell.btnCardDelete addTarget:self action:@selector(DeleteCard:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        selectRow=indexPath.row;
    }
    else
    {
        selectRow=indexPath.row+1;
    }
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"DEFAULT_CARD_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"CANCEL", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
    alert.tag=200;
    [alert show];
}

#pragma mark -
#pragma mark - DeleteCard WS

-(void)DeleteCard: (id)sender
{
    UIButton *btn=(UIButton *)sender;
    buttonTag=btn.tag-1;
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"CANCEL_CARD_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"CANCEL", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
    alert.tag=100;
    [alert show];
}
-(void)RemoveCard
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"REMOVEING_CARD", nil)];
        
        NSMutableDictionary *dict=[arrForCards objectAtIndex:buttonTag];
        
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        
        [dictParam setObject:strForUserId forKeyedSubscript:PARAM_ID];
        [dictParam setObject:strForUserToken forKeyedSubscript:PARAM_TOKEN];
        [dictParam setValue:[dict valueForKey:@"id"] forKey:PARAM_DEFAULT_CARD];
        
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_DELETE_CARD withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"REMOVE_CARD_SUCCESS", nil)];
                     [self getAllMyCards];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                     if([str isEqualToString:@"21"])
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:str];
                     }
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:str];
                     }
                     
                     if([str intValue] == 406)
                     {
                         [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                     }
                 }
             }
         }];
        
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

#pragma mark -
#pragma mark - Set DefaultCard WS

-(void)SetDefaultCard
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"SETING_DEFAULT_CARD", nil)];
        
        NSMutableDictionary *dict=[arrForCards objectAtIndex:selectRow];
        
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:strForUserId forKeyedSubscript:PARAM_ID];
        [dictParam setObject:strForUserToken forKeyedSubscript:PARAM_TOKEN];
        [dictParam setValue:[dict valueForKey:@"id"] forKey:@"default_card_id"];
        
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_SELECT_CARD withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"DEFAULT_CARD_SET_SUCCESS", nil)];
                     [self getAllMyCards];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_messages"]];
                     if([str isEqualToString:@"21"])
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:str];
                     }
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:str];
                     }
                     
                     NSString *str1 = [response valueForKey:@"error_code"];
                     
                     if([str1 intValue] == 406)
                     {
                         [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

#pragma mark -
#pragma mark - UIAlertView Delegate methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        switch (buttonIndex)
        {
            case 0:
                break;
            case 1:
                [self RemoveCard];
                break;
            default:
                break;
        }
    }
    else if(alertView.tag==200)
    {
        switch (buttonIndex)
        {
            case 0:
                break;
            case 1:
                [self SetDefaultCard];
                break;
            default:
                break;
        }
    }
}
- (IBAction)onClickBAck:(id)sender
{
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"fromRegister"];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end