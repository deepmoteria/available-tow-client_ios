//
//  PaymentVC.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 01/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import "BaseVC.h"
#import "PTKView.h"
#import "Stripe.h"
#import "PTKTextField.h"

@interface PaymentVC : BaseVC <UITextFieldDelegate,PTKViewDelegate,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{

}
@property (weak, nonatomic) IBOutlet UILabel *lblCardinfo;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnAddNewCard;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIButton *BtnAddCard;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)onClickBAck:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *txtCardNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtMonth;
@property (weak, nonatomic) IBOutlet UITextField *txtYear;
@property (weak, nonatomic) IBOutlet UITextField *txtCVC;

@property (weak, nonatomic) IBOutlet UIImageView *img_Display;
@property (weak, nonatomic) IBOutlet UIView *viewForDemo;

@property (weak, nonatomic) IBOutlet UIView *viewForAddingCard;
@property (weak, nonatomic) IBOutlet UITableView *TableObj;

- (IBAction)onClickBtnAddNewCard:(id)sender;
- (IBAction)onClickBtnMenu:(id)sender;
- (IBAction)onClickBtnAddCard:(id)sender;
- (IBAction)onClickBtnCloseView:(id)sender;
@end
