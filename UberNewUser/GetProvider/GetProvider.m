//
//  GetProvider.m
//  TaxiNow
//
//  Created by My Mac on 7/10/15.
//  Copyright (c) 2015 Jigs. All rights reserved.

#import "GetProvider.h"
#import "showdriverCell.h"
#import "ProviderDetailsVC.h"
#import "UIImageView+Download.h"
#import "ASStarRatingView.h"
#import "RatingBar.h"
#import "SWRevealViewController.h"
#import "UIView+Utils.h"
#import "UIImageView+Download.h"

@interface GetProvider()
{
    NSString *strForUserId,*strForUserToken,*strReqId,*strForDriverLatitude,*strForDriverLongitude;
    NSString *strForStripeToken,*strForLastFour;
    NSString *strProviderId;
}
@end

@implementation GetProvider
@synthesize arrProviders,strForLatitude,strForLongitude,strForTypeid,timerForCheckReqStatus,strPayment_Option;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self customSetup];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.viewForAddCard.hidden = YES;
    [[AppDelegate sharedAppDelegate]hideLoadingView];
    NSLog(@"arr = %@",arrProviders);
    self.btnCancelRequest.hidden = YES;
    //self.btnMenu = [APPDELEGATE setBoldFontDiscriptor:self.btnMenu];
}
-(void)viewWillAppear:(BOOL)animated
{
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    isMenuOpen=NO;
    [timerForCheckReqStatus invalidate];
    timerForCheckReqStatus=nil;
    self.btnCancelRequest.hidden=YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtCardNumber resignFirstResponder];
    [self.txtCVC resignFirstResponder];
    [self.txtMonth resignFirstResponder];
    [self.txtYear resignFirstResponder];
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController)
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        //[self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}

-(void)setText:(UILabel *)lbl
{
    lbl.numberOfLines = 0;
    lbl.lineBreakMode = NSLineBreakByWordWrapping;
    lbl.textAlignment = NSTextAlignmentLeft;
}

#pragma mark -
#pragma mark - UIAlertView Delegate methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1000)
    {
        switch (buttonIndex)
        {
            case 0:
                self.viewForAddCard.hidden = NO;
                break;
                
            default:
                break;
        }
    }
}


#pragma mark
#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrProviders.count;
}
-(showdriverCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    showdriverCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProviderList"];
    
    if(cell == nil)
    {
        cell = [[showdriverCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"ProviderList"];
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    dict = [arrProviders objectAtIndex:indexPath.row];
    
    cell.lblDriverName.text = [NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"first_name"],[dict valueForKey:@"last_name"]];
    cell.lblCompany.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"company_name"]];
    cell.lblAddress.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"address"]];
    
    [cell.imgDriverProfile downloadFromURL:[dict valueForKey:@"picture"] withPlaceholder:nil];
    
    cell.lblRate.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"rating"]];
    
    [cell.imgDriverProfile applyRoundedCornersFull];
    
    [self setText:cell.lblDriverName];
    [self setText:cell.lblCompany];
    [self setText:cell.lblAddress];
    
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CREATING_REQUEST", nil)];
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    strForUserId=[pref objectForKey:PREF_USER_ID];
    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
    
    NSDictionary *dict=[arrProviders objectAtIndex:indexPath.row];
    strProviderId=[dict valueForKey:@"id"];
    
    [pref setValue:strProviderId forKey:@"PID"];
    [pref synchronize];
    [self createRequest];
}
-(void)checkForDriverRequestStatus
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        strReqId=[pref objectForKey:PREF_REQ_ID];
        NSLog(@" q :%@",strReqId);
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 response = [[UtilityClass sharedObject] dictionaryByReplacingNullsWithStrings:response];

                 if([[response valueForKey:@"success"]boolValue] && [[response valueForKey:@"confirmed_walker"] integerValue]!=0)
                 {
                     NSLog(@"GET REQ--->%@",response);
                     NSString *strCheck=[response valueForKey:@"walker"];
                     
                     if(strCheck)
                     {
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         NSMutableDictionary *dictWalker=[response valueForKey:@"walker"];
                         strForDriverLatitude=[dictWalker valueForKey:@"latitude"];
                         strForDriverLongitude=[dictWalker valueForKey:@"longitude"];
                         if ([[response valueForKey:@"is_walker_rated"]integerValue]==1)
                         {
                             [pref removeObjectForKey:PREF_REQ_ID];
                             [pref synchronize];
                         }
                         
                         ProviderDetailsVC *vcFeed = nil;
                         for (int i=0; i<self.navigationController.viewControllers.count; i++)
                         {
                             UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
                             if ([vc isKindOfClass:[ProviderDetailsVC class]])
                             {
                                 vcFeed = (ProviderDetailsVC *)vc;
                             }
                         }
                         if (vcFeed==nil)
                         {
                             [timerForCheckReqStatus invalidate];
                             timerForCheckReqStatus=nil;
                             [[AppDelegate sharedAppDelegate]hideLoadingView];
                             
                             self.btnCancelRequest.hidden=YES;
                             //[self.btnCancelRequest removeFromSuperview];
                             [APPDELEGATE.window sendSubviewToBack:self.btnCancelRequest];
                             
                             [self performSegueWithIdentifier:SEGUE_TO_PROVIDER sender:self];
                         }
                         else
                         {
                             [timerForCheckReqStatus invalidate];
                             timerForCheckReqStatus=nil;
                             [[AppDelegate sharedAppDelegate]hideLoadingView];
                             [self.navigationController popToViewController:vcFeed animated:NO];
                         }
                     }
                 }
                 if([[response valueForKey:@"confirmed_walker"] intValue]==0 && [[response valueForKey:@"status"] intValue]==1)
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     [timerForCheckReqStatus invalidate];
                     timerForCheckReqStatus=nil;
                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                     [pref removeObjectForKey:PREF_REQ_ID];
                     [pref synchronize];
                     
                     if([[response valueForKey:@"is_cancelled"] intValue]==1)
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_TIMEOUT", nil)];
                     else
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"NO_WALKER", nil)];
                     
                     self.btnCancelRequest.hidden=YES;
                     // [self showMapCurrentLocatinn];
                     [APPDELEGATE hideLoadingView];
                 }
                 else
                 {
                     NSString *str = [response valueForKey:@"error_code"];
                     if([str intValue] == 406)
                     {
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus = nil;
                         [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                     }
                 }
             }
             else
             {
                 [APPDELEGATE hideLoadingView];
                 [timerForCheckReqStatus invalidate];
                 timerForCheckReqStatus=nil;
                 self.btnCancelRequest.hidden=YES;
                 //[self.btnCancelRequest removeFromSuperview];
                 //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                 //[alert show];
                 [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
             }
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:SEGUE_TO_PROVIDER])
    {
        
        ProviderDetailsVC *obj=[segue destinationViewController];
        obj.strForLatitude=strForLatitude;
        obj.strForLongitude=strForLongitude;
        obj.strForWalkStatedLatitude=strForDriverLatitude;
        obj.strForWalkStatedLongitude=strForDriverLongitude;
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - TextFieldDelegate Methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.txtCardNumber)
    {
        if (string==nil || [string isEqualToString:@""] || [string isKindOfClass:[NSNull class]] || string.length < 1)
        {
            
        }
        else if(self.txtCardNumber.text.length==4 || self.txtCardNumber.text.length==9 || self.txtCardNumber.text.length==14)
        {
            NSString *str=self.txtCardNumber.text;
            self.txtCardNumber.text=[NSString stringWithFormat:@"%@ ",str];
        }
        
        if (self.txtCardNumber.text.length == Card_Length && range.length == 0)
        {
            if(self.txtMonth.text.length >= Card_Month)
            {
                return NO;
            }
            else
            {
                [self.txtMonth becomeFirstResponder];
            }
        }
    }
    else if (textField == self.txtMonth)
    {
        if (self.txtMonth.text.length >= Card_Month && range.length == 0)
        {
            if(self.txtYear.text.length >= Card_Year)
            {
                return NO;
            }
            else
            {
                [self.txtYear becomeFirstResponder];
            }
        }
        
    }
    else if (textField == self.txtYear)
    {
        if (self.txtYear.text.length >= Card_Year && range.length == 0)
        {
            if(self.txtCVC.text.length >= Card_CVC_CVV)
            {
                return NO;
            }
            else
            {
                [self.txtCVC becomeFirstResponder];
            }
        }
    }
    else
    {
        if (self.txtCVC.text.length >= Card_CVC_CVV && range.length == 0)
        {
            [self.txtCVC resignFirstResponder];
        }
    }
    
    return YES;
}

#pragma mark
#pragma mark - action methods

-(void)createRequest
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:@"1" forKey:PARAM_DISTANCE];
    [dictParam setValue:strForLatitude forKey:PARAM_LATITUDE];
    [dictParam setValue:strForLongitude  forKey:PARAM_LONGITUDE];
    [dictParam setValue:strForUserId forKey:PARAM_ID];
    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
    [dictParam setValue:strForTypeid forKey:PARAM_TYPE];
    [dictParam setValue:strPayment_Option forKey:PARAM_PAYMENT_OPT];
    [dictParam setValue:strProviderId  forKey:@"provider_id"];
    
    NSLog(@" half dict = %@",dictParam);
    
    //[APPDELEGATE hideLoadingView];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_CREATE_REQUEST_PROVIDERS withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         //[[AppDelegate sharedAppDelegate]hideLoadingView];
         if (response)
         {
             response = [[UtilityClass sharedObject] dictionaryByReplacingNullsWithStrings:response];
             [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CREATING_REQUEST", nil)];

             NSLog(@"res = %@",response);
             if([[response valueForKey:@"success"]boolValue])
             {
                 NSUserDefaults *prefe = [NSUserDefaults standardUserDefaults];
                 NSString *strred=[response valueForKey:@"request_id"];
                 [prefe setValue:strred forKey:PREF_REQ_ID];
                 [prefe synchronize];
                 NSLog(@"req id :%@",strred);
                 
                 self.btnCancelRequest.hidden=NO;
                 
                 [APPDELEGATE.window addSubview:self.btnCancelRequest];
                 [APPDELEGATE.window bringSubviewToFront:self.btnCancelRequest];
                 
                 timerForCheckReqStatus = [NSTimer scheduledTimerWithTimeInterval:7.0 target:self selector:@selector(checkForDriverRequestStatus) userInfo:nil repeats:YES];
             }
             else
             {
                 [timerForCheckReqStatus invalidate];
                 timerForCheckReqStatus = nil;
                 
                 NSString *str = [response valueForKey:@"error_code"];
                 if([str intValue] == 406)
                 {
                     [timerForCheckReqStatus invalidate];
                     timerForCheckReqStatus = nil;
                     [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                 }
                 else
                 {
                     if([[response valueForKey:@"error_code"] intValue]==417)
                     {
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                         alert.tag = 1000;
                         [alert show];
                     }
                     else
                     {
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                         [alert show];
                     }
                 }
                 [APPDELEGATE hideLoadingView];
             }
             
         }
         else
         {
             [APPDELEGATE hideLoadingView];
         }
     }];
}

- (IBAction)onClickCancelRequest:(id)sender
{
    if([CLLocationManager locationServicesEnabled])
    {
        if([[AppDelegate sharedAppDelegate]connected])
        {
            [[AppDelegate sharedAppDelegate]hideLoadingView];
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CANCLEING", nil)];
            
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            strForUserId=[pref objectForKey:PREF_USER_ID];
            strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
            NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            
            [dictParam setValue:strForUserId forKey:PARAM_ID];
            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setValue:strReqId forKey:PARAM_REQUEST_ID];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CANCEL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if (response)
                 {
                     response = [[UtilityClass sharedObject] dictionaryByReplacingNullsWithStrings:response];

                     if([[response valueForKey:@"success"]boolValue])
                     {
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus=nil;
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         //[self.btnCancelRequest removeFromSuperview];
                         [self.btnCancelRequest setHidden:YES];
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCEL", nil)];
                         [self.navigationController popViewControllerAnimated:YES];
                     }
                     else
                     {
                         NSString *str = [response valueForKey:@"error_code"];
                         if([str intValue] == 406)
                         {
                             [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                         }
                     }
                 }
                 
                 
             }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
        
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Towber -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
        
        
    }
    
}
- (IBAction)onClickAddCard:(id)sender
{
    [self.view endEditing:YES];
    
    if(self.txtCardNumber.text.length<1 || self.txtMonth.text.length<1 || self.txtYear.text.length<1 || self.txtCVC.text.length<1)
    {
        if(self.txtCardNumber.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_CREDIT_CARD_NUMBER", nil)];
        }
        else if(self.txtMonth.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_MONTH", nil)];
        }
        else if(self.txtYear.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_YEAR", nil)];
        }
        else if(self.txtCVC.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLESE_CVV", nil)];
        }
    }
    else
    {
        if (![Stripe defaultPublishableKey])
        {
            [[UtilityClass sharedObject] showAlertWithTitle:NSLocalizedString(@"NO_PUBLISH_KEY", nil) andMessage:NSLocalizedString(@"PUBLIS_MESSAGE", nil)];
            
            return;
        }
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"ADDING_CARD", nil)];
        STPCard *card = [[STPCard alloc] init];
        
        NSString *strCard=[self.txtCardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        card.number =strCard;
        card.expMonth =[self.txtMonth.text integerValue];
        card.expYear = [self.txtYear.text integerValue];
        card.cvc = self.txtCVC.text;
        
        [Stripe createTokenWithCard:card completion:^(STPToken *token, NSError *error) {
            if (error)
            {
                [[AppDelegate sharedAppDelegate] hideLoadingView];
                [self hasError:error];
            }
            else
            {
                [self hasToken:token];
                [self addCardOnServer];
            }
        }];
        
    }
}

- (void)hasError:(NSError *)error
{
    [[UtilityClass sharedObject] showAlertWithTitle:NSLocalizedString(@"Error", @"Error") andMessage:[error.userInfo valueForKey:@"com.stripe.lib:ErrorMessageKey"]];
    [APPDELEGATE hideLoadingView];
}

- (void)hasToken:(STPToken *)token
{
    strForLastFour=token.card.last4;
    strForStripeToken=token.tokenId;
}
-(void)addCardOnServer
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:strForUserId forKey:PARAM_ID];
        [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
        [dictParam setValue:strForStripeToken forKey:PARAM_STRIPE_TOKEN];
        [dictParam setValue:strForLastFour forKey:PARAM_LAST_FOUR];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_ADD_CARD withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [APPDELEGATE hideLoadingView];
             if(response)
             {
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if ([[response valueForKey:@"success"] boolValue])
                 {
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"ADD_CARD_SUCCESS", nil)];
                     self.txtCardNumber.text=@"";
                     self.txtMonth.text=@"";
                     self.txtYear.text=@"";
                     self.txtCVC.text=@"";
                     self.viewForAddCard.hidden=YES;
                     [self createRequest];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_messages"]];
                     
                     if([str intValue] == 406)
                     {
                         [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                     }
                     else
                     {
                         NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:str];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
    
}

- (IBAction)onClickCloseCardView:(id)sender
{
    [self.view endEditing:YES];
    self.txtCardNumber.text=@"";
    self.txtMonth.text=@"";
    self.txtYear.text=@"";
    self.txtCVC.text=@"";
    self.viewForAddCard.hidden=YES;
}

@end