//
//  GetProvider.h
//  TaxiNow
//
//  Created by My Mac on 7/10/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import "RatingBar.h"
#import "ASStarRatingView.h"
#import "PTKView.h"
#import "Stripe.h"
#import "PTKTextField.h"


@interface GetProvider : BaseVC<UITableViewDataSource,PTKViewDelegate ,UITableViewDelegate>
{
   
}
@property (weak, nonatomic) IBOutlet UITableView *tblProviderList;
@property(strong,nonatomic)NSMutableArray *arrProviders;
@property (strong , nonatomic) NSTimer *timerForCheckReqStatus;
@property (weak, nonatomic) IBOutlet UIView *viewForAddCard;

@property (strong,nonatomic)NSString *strForLatitude;
@property (strong,nonatomic)NSString *strForLongitude;
@property (strong,nonatomic)NSString *strForTypeid;
@property (strong,nonatomic)NSString *strPayment_Option;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelRequest;
- (IBAction)onClickCancelRequest:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
- (IBAction)onClickAddCard:(id)sender;
- (IBAction)onClickCloseCardView:(id)sender;


@property (weak, nonatomic) IBOutlet UITextField *txtCardNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtMonth;
@property (weak, nonatomic) IBOutlet UITextField *txtYear;
@property (weak, nonatomic) IBOutlet UITextField *txtCVC;


@end
