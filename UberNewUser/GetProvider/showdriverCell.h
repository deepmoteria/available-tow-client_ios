//
//  showdriverCell.h
//  TaxiNow
//
//  Created by My Mac on 7/10/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingBar.h"
#import "ASStarRatingView.h"

@interface showdriverCell : UITableViewCell
{
    RatingBar *ratingView;

}
@property (weak, nonatomic) IBOutlet UIImageView *imgDriverProfile;

@property (weak, nonatomic) IBOutlet UILabel *lblDriverName;
@property (weak, nonatomic) IBOutlet UILabel *lblCompany;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet ASStarRatingView *starView;
@property (weak, nonatomic) IBOutlet UILabel *lblRate;



@end
