//
//  TypesCell.h
//  Available Tow Client
//
//  Created by My Mac on 11/04/16.
//  Copyright © 2016 Jigs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TypesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTypeName;
@property (weak, nonatomic) IBOutlet UILabel *lblTypePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblDivider;

@end
