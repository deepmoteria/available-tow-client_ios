//  FeedBackVC.m
//  UberNewUser
//  Created by Deep Gami on 01/11/14.
//  Copyright (c) 2014 Jigs. All rights reserved.

#import "FeedBackVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "PickUpVC.h"
#import "UberStyleGuide.h"

@interface FeedBackVC ()
{
    NSMutableArray *arrSubTypes,*strForCurrency;
}
@end

@implementation FeedBackVC

@synthesize ratingView;

#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // [super setBackBarItem];
    
    [self SetLocalization];
    [self customFont];
    [ratingView initRateBar];
    
    NSArray *arrName=[self.strFirstName componentsSeparatedByString:@" "];
    
    self.lblFirstName.text= [NSString stringWithFormat:@"%@ %@",[arrName objectAtIndex:0],[arrName objectAtIndex:1]];
    self.lblLastName.text=[arrName objectAtIndex:1];
    
    self.lblDistance.textColor=[UberStyleGuide colorDefault];
    self.lblTIme.textColor=[UberStyleGuide colorDefault];
    
    self.lblDistance.text=[NSString stringWithFormat:@"%.2f",[[self.dictWalkInfo valueForKey:@"distance"] floatValue]];
    self.lblTIme.text=[self.dictWalkInfo valueForKey:@"time"];
    [self.imgUser applyRoundedCornersFull];
    [self.imgUser downloadFromURL:self.strUserImg withPlaceholder:nil];
    self.viewForBill.hidden=NO;
    self.txtComments.text=NSLocalizedString(@"COMMENT", nil);
    [self customSetup];
    self.lblTotal.text=[NSString stringWithFormat:@"%@ %.2f",[dictBillInfo valueForKey:@"currency"],[[dictBillInfo valueForKey:@"total"] floatValue]];
    [self setPriceValue];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"dict bill info %@",dictBillInfo);
    
    arrSubTypes = [[NSMutableArray alloc]init];
    [arrSubTypes addObjectsFromArray:[dictBillInfo valueForKey:@"type"]];
    
    strForCurrency = [dictBillInfo valueForKey:@"currency"];
    
    //NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    NSMutableDictionary *dictprice = [[NSMutableDictionary alloc]init];
    
    [dictprice setValue:[dictBillInfo valueForKey:@"price_per_unit_time"] forKey:@"time_price"];
    [dictprice setValue:NSLocalizedString(@"Time Cost :", nil) forKey:@"name"];
    [dictprice setValue:[dictBillInfo valueForKey:@"time_cost"] forKey:@"cost"];
    [dictprice setValue:[dictBillInfo valueForKey:@"currency"] forKey:@"currency"];
    [arrSubTypes addObject:dictprice];
    
    dictprice = [[NSMutableDictionary alloc]init];
    
    [dictprice setValue:[dictBillInfo valueForKey:@"price_per_unit_distance"] forKey:@"distance_price"];
    [dictprice setValue:NSLocalizedString(@"Distance Cost :", nil) forKey:@"name"];
    [dictprice setValue:[dictBillInfo valueForKey:@"currency"] forKey:@"currency"];
    [dictprice setValue:[dictBillInfo valueForKey:@"distance_cost"] forKey:@"cost"];
    [arrSubTypes addObject:dictprice];
    
    dictprice = [[NSMutableDictionary alloc]init];
    [dictprice setValue:NSLocalizedString(@"PROMO_BONUS", nil) forKey:@"name"];
    [dictprice setValue:[dictBillInfo valueForKey:@"currency"] forKey:@"currency"];
    [dictprice setValue:[dictBillInfo valueForKey:@"promo_bonus"] forKey:@"price"];
    [arrSubTypes addObject:dictprice];
    
    dictprice = [[NSMutableDictionary alloc]init];
    [dictprice setValue:NSLocalizedString(@"REFERRAL_BONUS", nil) forKey:@"name"];
    [dictprice setValue:[dictBillInfo valueForKey:@"currency"] forKey:@"currency"];
    [dictprice setValue:[dictBillInfo valueForKey:@"referral_bonus"] forKey:@"price"];
    [arrSubTypes addObject:dictprice];
    
    dictprice = [[NSMutableDictionary alloc]init];
    [dictprice setValue:NSLocalizedString(@"province_tax", nil) forKey:@"name"];
    [dictprice setValue:[dictBillInfo valueForKey:@"currency"] forKey:@"currency"];
    [dictprice setValue:[dictBillInfo valueForKey:@"province_tax"] forKey:@"price"];
    [arrSubTypes addObject:dictprice];
    
    dictprice = [[NSMutableDictionary alloc]init];
    [dictprice setValue:NSLocalizedString(@"CANADA_TAX", nil) forKey:@"name"];
    [dictprice setValue:[dictBillInfo valueForKey:@"currency"] forKey:@"currency"];
    [dictprice setValue:[dictBillInfo valueForKey:@"canada_tax"] forKey:@"price"];
    [arrSubTypes addObject:dictprice];
    
    self.tblForTypes.frame = CGRectMake(self.tblForTypes.frame.origin.x, self.tblForTypes.frame.origin.y, self.tblForTypes.frame.size.width,self.tblForTypes.frame.size.width+arrSubTypes.count+45.0f);
    
    if([[dictBillInfo valueForKey:@"is_paid"] intValue]==0 && [dictBillInfo valueForKey:@"total"]>0)
    {
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"" message:@"Payment process has been failed due to your card or server problem so please try again with valid card detail." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Make Payment",@"Add New Card", nil];
        alert.tag=2000;
        [alert show];
    }
}
- (void)viewDidAppear:(BOOL)animated
{
    [[AppDelegate sharedAppDelegate] hideLoadingView];
    
    [self.btnFeedBack setTitle:NSLocalizedString(@"Invoice", nil) forState:UIControlStateNormal];
}
-(void)SetLocalization
{
    self.lblInvoice.text=NSLocalizedString(@"Invoice", nil);
    self.lBasePrice.text=NSLocalizedString(@"BASE PRICE", nil);
    self.lDistanceCost.text=NSLocalizedString(@"DISTANCE COST", nil);
    self.lTimeCost.text=NSLocalizedString(@"TIME COST", nil);
    self.lPromoBonus.text=NSLocalizedString(@"PROMO BOUNCE", nil);
    self.lreferalBonus.text=NSLocalizedString(@"REFERRAL BOUNCE", nil);
    self.lTotalCost.text=NSLocalizedString(@"Total Due", nil);
    self.lComment.text=NSLocalizedString(@"COMMENT", nil);
    [self.btnConfirm setTitle:NSLocalizedString(@"CONFIRM", nil) forState:UIControlStateNormal];
    [self.btnSubmit setTitle:NSLocalizedString(@"Submit", nil) forState:UIControlStateNormal];
}
#pragma mark-
#pragma mark- Set Invoice Details


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==2000)
    {
        NSLog(@"index:%ld",(long)buttonIndex);
        if(buttonIndex == 0)
        {
            [self checkPaymentService];
        }
        else
        {
            UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
            
            PickUpVC *ViewObj;
            
            ViewObj=(PickUpVC *)[nav.childViewControllers objectAtIndex:0];
            if(ViewObj!=nil)
                [ViewObj goToSetting:@"segueToPayment" withIndex:3];
        }
    }
}

-(void)setPriceValue
{
    
    self.lblBasePrice.text=[NSString stringWithFormat:@"$ %@",[dictBillInfo valueForKey:@"base_price"]];
    self.lblDistCost.text=[NSString stringWithFormat:@"$ %@",[dictBillInfo valueForKey:@"distance_cost"]];
    self.lblTimeCost.text=[NSString stringWithFormat:@"$ %.2f",[[dictBillInfo valueForKey:@"time_cost"] floatValue]];
    // self.lblTotal.text=[NSString stringWithFormat:@"$ %.2f",[[dictBillInfo valueForKey:@"total"] floatValue]];
    self.lblRferralBouns.text=[NSString stringWithFormat:@"$ %.2f",[[dictBillInfo valueForKey:@"referral_bonus"] floatValue]];
    self.lblPromoBouns.text=[NSString stringWithFormat:@"$ %.2f",[[dictBillInfo valueForKey:@"promo_bonus"] floatValue]];
    float totalDist=[[dictBillInfo valueForKey:@"distance_cost"] floatValue];
    float Dist=[[dictBillInfo valueForKey:@"distance"]floatValue];
    
    if ([[dictBillInfo valueForKey:@"unit"]isEqualToString:@"kms"])
    {
        totalDist=totalDist*0.621317;
        Dist=Dist*0.621371;
    }
    if(Dist!=0)
    {
        self.lblPerDist.text=[NSString stringWithFormat:@"%.2f$ %@",(totalDist/Dist),NSLocalizedString(@"per mile", nil)];
    }
    else
    {
        self.lblPerDist.text=[NSString stringWithFormat:@"0$ %@",NSLocalizedString(@"per mile", nil)];
    }
    
    float totalTime=[[dictBillInfo valueForKey:@"time_cost"] floatValue];
    float Time=[[dictBillInfo valueForKey:@"time"]floatValue];
    if(Time!=0)
    {
        self.lblPerTime.text=[NSString stringWithFormat:@"%.2f$ %@",(totalTime/Time),NSLocalizedString(@"per mins", nil)];
    }
    else
    {
        self.lblPerTime.text=[NSString stringWithFormat:@"0$ %@",NSLocalizedString(@"per mins", nil)];
    }
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController)
    {
        [self.btnFeedBack addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)checkPaymentService
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        
        [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setValue:[USERDEFAULT objectForKey:PREF_REQ_ID] forKey:PARAM_REQUEST_ID];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_CHECK_PAYMENT withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Payment status--->%@",response);
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 response = [[UtilityClass sharedObject] dictionaryByReplacingNullsWithStrings:response];

                 if([[response valueForKey:@"success"]boolValue])
                 {
                     [APPDELEGATE showToastMessage:@"Your payment has been succesfully done"];
                 }
                 else
                 {
                     NSString *str = [response valueForKey:@"error_code"];
                     if([str intValue] == 406)
                     {
                         [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                     }
                     else
                     {
                         UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"" message:@"Payment process has been failed due to your card or server problem so please try again with valid card detail." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Make Payment",@"Add New Card", nil];
                         alert.tag=2000;
                         [alert show];
                     }
                 }
             }
         }];
    }
    
}

#pragma mark-
#pragma mark- Custom Font

-(void)customFont
{
    /*self.lblDistance.font=[UberStyleGuide fontRegular];
     self.lblDistCost.font=[UberStyleGuide fontRegular];
     self.lblBasePrice.font=[UberStyleGuide fontRegular];
     self.lblDistance.font=[UberStyleGuide fontRegular];
     self.lblPerDist.font=[UberStyleGuide fontRegular];
     self.lblPerTime.font=[UberStyleGuide fontRegular];
     self.lblTIme.font=[UberStyleGuide fontRegular];
     self.lblTimeCost.font=[UberStyleGuide fontRegular];
     self.lblTotal.font=[UberStyleGuide fontRegular:30.0f];
     self.lblFirstName.font=[UberStyleGuide fontRegular];
     self.lblLastName.font=[UberStyleGuide fontRegular];
     self.btnFeedBack.titleLabel.font=[UberStyleGuide fontRegular];
     self.lblPromoBouns.font=[UberStyleGuide fontRegular];
     self.lblRferralBouns.font=[UberStyleGuide fontRegular];
     self.btnFeedBack = [APPDELEGATE setBoldFontDiscriptor:self.btnFeedBack];
     self.btnSubmit.titleLabel.font=[UberStyleGuide fontRegularBold];*/
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSubTypes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"subType";
    
    subTypeCell *cell = [self.tblForTypes dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell==nil)
    {
        cell=[[subTypeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSDictionary *dict = [arrSubTypes objectAtIndex:indexPath.row];
    
    NSInteger count = (arrSubTypes.count-indexPath.row);
    
    if(count==5)
    {
        cell.lblMainType.hidden=NO;
        cell.lblSubType.hidden=NO;
        cell.lblTypeName.hidden=YES;
        cell.lblMainType.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"name"]];
        cell.lblSubType.text = [NSString stringWithFormat:@"%@ %@ per km",[dict valueForKey:@"currency"],[dict valueForKey:@"distance_price"]];
        cell.lblPrice.text =[NSString stringWithFormat:@"%@ %.2f",[dict valueForKey:@"currency"],[[dict valueForKey:@"cost"] floatValue]];
    }
    else if (count==6)
    {
        cell.lblMainType.hidden=NO;
        cell.lblSubType.hidden=NO;
        cell.lblTypeName.hidden=YES;
        cell.lblMainType.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"name"]];
        cell.lblSubType.text = [NSString stringWithFormat:@"%@ %@ per min",[dict valueForKey:@"currency"],[dict valueForKey:@"time_price"]];
        cell.lblPrice.text =[NSString stringWithFormat:@"%@ %.2f",[dict valueForKey:@"currency"],[[dict valueForKey:@"cost"] floatValue]];
    }
    else
    {
        cell.lblMainType.hidden=YES;
        cell.lblSubType.hidden=YES;
        cell.lblTypeName.hidden=NO;
        cell.lblTypeName.text = [dict valueForKey:@"name"];
        cell.lblPrice.text =[NSString stringWithFormat:@"%@ %.2f",strForCurrency,[[dict valueForKey:@"price"] floatValue]];
    }
    
    return cell;
}

#pragma mark-
#pragma makr- Btn Click Events

- (IBAction)submitBtnPressed:(id)sender
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REVIEWING", nil)];
        
        RBRatings rating=[ratingView getcurrentRatings];
        
        float rate=rating/2.0;
        if (rating%2 != 0)
        {
            rate += 0.5;
        }
        if(rate==0)
        {
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"PLEASE_RATE", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
            
        }
        else
        {
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
            NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
            NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setValue:strForUserId forKey:PARAM_ID];
            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setValue:strReqId forKey:PARAM_REQUEST_ID];
            [dictParam setValue:[NSString stringWithFormat:@"%f",rate] forKey:PARAM_RATING];
            NSString *commt=self.txtComments.text;
            if([commt isEqualToString:NSLocalizedString(@"COMMENT", nil)])
            {
                [dictParam setValue:@"" forKey:PARAM_COMMENT];
            }
            else
            {
                [dictParam setValue:self.txtComments.text forKey:PARAM_COMMENT];
            }
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_RATE_DRIVER withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 NSLog(@"%@",response);
                 if (response)
                 {
                     response = [[UtilityClass sharedObject] dictionaryByReplacingNullsWithStrings:response];

                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"RATING", nil)];
                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:PREF_REQ_ID];
                         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"FromFeedback"];
                         [self.navigationController popToRootViewControllerAnimated:YES];
                         
                     }
                     else
                     {
                         NSString *str = [response valueForKey:@"error_code"];
                         if([str intValue] == 406)
                         {
                             [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                         }
                     }
                 }
                 
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 
             }];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    
    
}
#pragma mark -
#pragma mark - UITextField Delegate


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(0, -150, self.view.frame.size.width, self.view.frame.size.height);
        
        
    } completion:^(BOOL finished)
     {
     }];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished)
     {
     }];
    
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)confirmBtnPressed:(id)sender
{
    [self.btnFeedBack setTitle:NSLocalizedString(@"Feedback", nil) forState:UIControlStateNormal];
    self.viewForBill.hidden=YES;
    //ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(160, 30) AndPosition:CGPointMake(80, 252)];
}

#pragma mark-
#pragma mark- Text Field Delegate

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtComments resignFirstResponder];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.txtComments.text=@"";
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComments)
            {
                UITextPosition *beginning = [self.txtComments beginningOfDocument];
                [self.txtComments setSelectedTextRange:[self.txtComments textRangeFromPosition:beginning
                                                                                    toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComments)
            {
                UITextPosition *beginning = [self.txtComments beginningOfDocument];
                [self.txtComments setSelectedTextRange:[self.txtComments textRangeFromPosition:beginning
                                                                                    toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComments)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComments)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
    if ([self.txtComments.text isEqualToString:@""])
    {
        self.txtComments.text=NSLocalizedString(@"COMMENT", nil);
    }
    
}

#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
